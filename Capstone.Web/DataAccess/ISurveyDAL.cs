﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Web.DataAccess
{
    public interface ISurveyDAL
    {

        void WriteResultToFile(string parkCode);

        Dictionary<string,int> ReadInResults();
    }
}
