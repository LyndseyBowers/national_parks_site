﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Capstone.Web.DataAccess
{
    public class SurveyFileDAL : ISurveyDAL
    {
        private ILog logger = LogManager.GetLogger(typeof(ParksFileDAL));

        public string FileName { get; private set; }

        public SurveyFileDAL(string fileName)
        {
            FileName = fileName;
            ReadInResults();
        }

        public Dictionary<string, int> ReadInResults()
        {
            Dictionary<string, int> results = new Dictionary<string, int>();
            try
            {
                using (StreamReader sr = new StreamReader(FileName))
                {
                    
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if(results.ContainsKey(line))
                        {
                            results[line]++;
                        }
                        else
                        {
                            results.Add(line, 1);
                        }

                    }                       
                }
            }
            catch(IOException e)
            {
                logger.Error(e.Message);
            }

            return results;
        }


        public void WriteResultToFile(string parkCode)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(FileName, true))
                {
                    sw.WriteLine(parkCode);
                }
            }
            catch(IOException e)
            {
                logger.Error(e.Message);
            }
        }
    }
}