﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Web.DataAccess
{
    public interface IParksDAL
    {
        List<Park> GetAllParks();
        Park GetPark(string parkcode);
    }
}
