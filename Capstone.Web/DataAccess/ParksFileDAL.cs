﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Capstone.Web.Models;
using System.IO;
using log4net;

namespace Capstone.Web.DataAccess
{
    public class ParksFileDAL : IParksDAL
    {
        private List<Park> parks = new List<Park>();

        private string parksFile;
        private const int Index_ParkCode = 0;
        private const int Index_ParkName = 1;
        private const int Index_State = 2;
        private const int Index_Acreage = 3;
        private const int Index_Elevation = 4;
        private const int Index_MilesOfTrail = 5;
        private const int Index_CampSites = 6;
        private const int Index_Climate = 7;
        private const int Index_Year = 8;
        private const int Index_Visitors = 9;
        private const int Index_Quote = 10;
        private const int Index_QuoteSource = 11;
        private const int Index_Description = 12;
        private const int Index_Fee = 13;
        private const int Index_Species = 14;

        private ILog logger = LogManager.GetLogger(typeof(ParksFileDAL));

        public ParksFileDAL(string filepath)
        {
            //Call to read in list of Park
            parksFile = filepath;
            ReadInParksFile();
        }

        private void ReadInParksFile()
        {
            try
            {
                //uses parksFile to open a streamreader
                using (StreamReader sr = new StreamReader(parksFile))
                {
                    sr.ReadLine();

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] fields = line.Split('\t');

                        Park p = new Park()
                        {
                            ParkCode = fields[Index_ParkCode],
                            ParkName = fields[Index_ParkName],
                            State = fields[Index_State],
                            Acres = int.Parse(fields[Index_Acreage]),
                            Elevation = int.Parse(fields[Index_Elevation]),
                            MilesOfTrail = double.Parse(fields[Index_MilesOfTrail]),
                            Campsites = int.Parse(fields[Index_CampSites]),
                            Climate = fields[Index_Climate],
                            YearEstablished = int.Parse(fields[Index_Year]),
                            VisitorCount = int.Parse(fields[Index_Visitors]),
                            Quote = fields[Index_Quote],
                            QuoteSource = fields[Index_QuoteSource],
                            Description = fields[Index_Description],
                            EntryFee = double.Parse(fields[Index_Fee].Substring(1)),
                            NumberOfSpecies = int.Parse(fields[Index_Species]),

                        };
                        p.ImageFile = p.ParkCode.ToLower().Trim() + ".jpg";
                        //adds each park to the List<Park> parks field
                        parks.Add(p);
                    }
                    
                }

            }
            catch (IOException e)
            {
                logger.Error(e.Message);

            }
        }

        public List<Park> GetAllParks()
        {
            return parks;
        }

        public Park GetPark(string parkcode)
        {
            Park p = null;
            foreach(Park park in parks)
            {
                if(parkcode.ToLower().Trim() == park.ParkCode.ToLower().Trim())
                {
                    p = park;
                    break;
                }
            }
            return p;
        }
    }
}