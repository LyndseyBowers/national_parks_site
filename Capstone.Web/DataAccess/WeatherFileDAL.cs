﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Capstone.Web.Models;
using System.IO;
using log4net;

namespace Capstone.Web.DataAccess
{
    public class WeatherFileDAL : IWeatherDAL
    {
        private ILog logger = LogManager.GetLogger(typeof(ParksFileDAL));

        private List<DailyForecast> allForecasts = new List<DailyForecast>();
        private string weatherFile;
        private const int Index_ParkCode = 0;
        private const int Index_DayNumber = 1;
        private const int Index_LowTemp = 2;
        private const int Index_HighTemp = 3;
        private const int Index_Forecast = 4;

        public WeatherFileDAL(string filePath)
        {
            weatherFile = filePath;
            ReadInWeatherFile();
        }

        private void ReadInWeatherFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(weatherFile))
                {
                    sr.ReadLine();
                    while(!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] fields = line.Split('\t');

                        DailyForecast df = new DailyForecast()
                        {
                            ParkCode = fields[Index_ParkCode],
                            DayNumber = int.Parse(fields[Index_DayNumber]),
                            LowTemperature = int.Parse(fields[Index_LowTemp]),
                            HighTemperature = int.Parse(fields[Index_HighTemp]),
                            Forecast = fields[Index_Forecast]
                        };
                        allForecasts.Add(df);
                    }
                }

            }
            catch(IOException e)
            {
                logger.Error(e.Message);
            }

        }

        public List<DailyForecast> Get5DayForecast(string parkcode)
        {
            List<DailyForecast> fiveDayForecast = new List<DailyForecast>();
            foreach (var f in allForecasts)
            {
                if(f.ParkCode.ToLower().Trim() == parkcode.ToLower().Trim())
                {
                    fiveDayForecast.Add(f);
                }

            }
            return fiveDayForecast;
        }


       
    }
}