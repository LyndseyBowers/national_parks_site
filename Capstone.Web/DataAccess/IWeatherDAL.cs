﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Web.DataAccess
{
    public interface IWeatherDAL
    {
        List<DailyForecast> Get5DayForecast(string parkcode);

    }
}
