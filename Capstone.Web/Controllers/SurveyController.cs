﻿using Capstone.Web.DataAccess;
using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Capstone.Web.Controllers
{
    public class SurveyController : Controller
    {

        string Session_Key;
        SurveyResults results = new SurveyResults();

        private ISurveyDAL surveyDAL;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (surveyDAL == null)
            {
                string fileName = Server.MapPath("~/App_Data/Survey_Results.tsv");
                surveyDAL = new SurveyFileDAL(fileName);
            }
        }

        public SurveyController()
        {

        }

        public SurveyController(ISurveyDAL dal)
        {
            surveyDAL = dal;
        }

        [HttpGet]
        public ActionResult Create()
        {
            //if SessionID exists, return view for results.  If not, return Create view
            if (Session[Session_Key] != null)
            {               
                return RedirectToAction("Results");
            }
            else
            {
                Session[Session_Key] = "xyz";
                return View();
            }
            
        }

        [HttpPost]
        public ActionResult Create(string parkCode)
        {
            surveyDAL.WriteResultToFile(parkCode);
            return RedirectToAction("Results");
        }

        public ActionResult Results()
        {
            Dictionary<string, int> resultsDictionary = surveyDAL.ReadInResults();

            results.Results = resultsDictionary;
            
            return View(results);
        }

    }
}