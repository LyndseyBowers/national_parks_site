﻿using Capstone.Web.DataAccess;
using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace Capstone.Web.Controllers
{
    public class ParksController : Controller
    {

        private IParksDAL parkDAL;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (parkDAL == null)
            {
                string fileName = Server.MapPath("~/App_Data/Parks.tsv");
                parkDAL = new ParksFileDAL(fileName);
            }
        }

        public ParksController()
        {

        }
        
        public ParksController(IParksDAL dal)
        {
            parkDAL = dal;
        }

        public ActionResult Detail(string id)
        {
            Park p = parkDAL.GetPark(id);

            return View(p);
        }


    }
}