﻿using Capstone.Web.DataAccess;
using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Capstone.Web.Controllers
{
    public class WeatherController : Controller
    {
        private IWeatherDAL weatherDAL;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (weatherDAL == null)
            {
                string fileName = Server.MapPath("~/App_Data/Weather.tsv");
                weatherDAL = new WeatherFileDAL(fileName);
            }
        }

        public WeatherController()
        {

        }

        public WeatherController(IWeatherDAL dal)
        {
            weatherDAL = dal;
        }

        public ActionResult Detail(string id)
        {
            List<DailyForecast> df = weatherDAL.Get5DayForecast(id);

            return View(df);
        }

        public ActionResult Celcius(string id)
        {
            List<DailyForecast> df = weatherDAL.Get5DayForecast(id);

            return View(df);
        }
    }
}