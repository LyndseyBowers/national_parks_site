﻿using Capstone.Web.DataAccess;
using Capstone.Web.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Capstone.Web.Controllers
{
    public class HomeController : Controller
    {
        //creating an instance of the logger
        //Use LogManager.GetLogger(typeof(CLASSNAME))
        private ILog logger = LogManager.GetLogger(typeof(HomeController));
        private IParksDAL parksDAL;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (parksDAL == null)
            {
                string fileName = Server.MapPath("~/App_Data/Parks.tsv");
                parksDAL = new ParksFileDAL(fileName);
            }
        }


        // GET: Home
        public ActionResult Index()
        {            
            List<Park> allParks = parksDAL.GetAllParks();

            logger.Debug("Entering index method");
            return View(allParks);
        }
    }
}