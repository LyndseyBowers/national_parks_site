﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Capstone.Web.Models
{
    public class Park
    {
        public string ParkCode { get; set; }
        public string ParkName { get; set; }
        public string State { get; set; }
        public int YearEstablished { get; set; }
        public int Acres { get; set; }
        public double MilesOfTrail { get; set; }
        public int Elevation { get; set; }
        public int Campsites { get; set; }
        public string Climate { get; set; }
        public string Quote { get; set; }
        public string QuoteSource { get; set; }
        public string Description { get; set; }
        public int VisitorCount { get; set; }
        public double EntryFee { get; set; }
        public int NumberOfSpecies { get; set; }
        public string ImageFile { get; set; }

    }
}