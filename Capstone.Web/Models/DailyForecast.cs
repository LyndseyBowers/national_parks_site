﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Capstone.Web.Models
{
    public class DailyForecast
    {
        private string snowString = "It's coming down! Bring your snowshoes!";
        private string rainString = "You're going to want rain gear and waterproof shoes today!";
        private string thunderString = "Thunderstorm warning, seek shelter!";
        private string sunnyString = "It's a beautiful day!  Please remember to wear your sunblock!";
        private string highTempString = "It's going to be hot!  Bring extra water!";
        private string bigDiffString = "Big temperature variations. Wear breathable layers!";
        private string below20String = "Frigid temps! Beware of frostbite!";

        public string ParkCode { get; set; }
        public int LowTemperature { get; set; }
        public int HighTemperature { get; set; }
        public string Forecast { get; set; }
        public int DayNumber { get; set; }
        public double CelciusLow { get { return Math.Round((LowTemperature - 32) * (5 / 9.0), 0); } }
        public double CelciusHigh { get { return Math.Round((HighTemperature - 32) * (5 / 9.0), 0); } }
        public string WeatherImgFile
        {
            get
            {
                if (Forecast == "cloudy")
                {
                    return "/Content/img/weather/cloudy.png";
                }
                if (Forecast == "partly cloudy")
                {
                    return "/Content/img/weather/partly-cloudy.png";
                }
                if (Forecast == "rain")
                {
                    return "/Content/img/weather/rain.png";
                }
                if (Forecast == "snow")
                {
                    return "/Content/img/weather/snow.png";
                }
                if (Forecast == "sunny")
                {
                    return "/Content/img/weather/sunny.png";
                }
                else
                {
                    return "/Content/img/weather/thunderstorms.png";
                }
            }
        }

        public string ForecastAdvice
        {
            get
            {
                if (Forecast == "snow")
                {
                    return snowString;
                }
                if (Forecast == "rain")
                {
                    return rainString;
                }
                if (Forecast == "thunderstorms")
                {
                    return thunderString;
                }
                if (Forecast == "sunny")
                {
                    return sunnyString;
                }
                else
                {
                    return null;
                }
            }
        }
        public string TempAdvice
        {
            get
            {
                if (HighTemperature >= 75)
                {
                    return highTempString;
                }
                if (HighTemperature - LowTemperature > 20)
                {
                    return bigDiffString;
                }
                if (LowTemperature < 20)
                {
                    return below20String;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}