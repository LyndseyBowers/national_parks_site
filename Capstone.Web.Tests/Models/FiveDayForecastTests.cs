﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Capstone.Web.Models.Tests
{
    [TestClass()]
    public class FiveDayForecastTests
    {
        [TestMethod()]
         public void GetForecastTest_ReturnDailyForecastObject_ExpectNotNull()
        {
            //Arrange
            List<DailyForecast> dailyforecast = new List<DailyForecast>
            {
                new DailyForecast {DayNumber = 1, Forecast ="misc" , HighTemperature = 100, LowTemperature = 0}
            };
                        
            FiveDayForecast f = new FiveDayForecast( dailyforecast,"ABC");
            
            //Act
            DailyForecast output = f.GetForecast(1);

            //Assert
            Assert.IsNotNull(output);
        }
    }
}